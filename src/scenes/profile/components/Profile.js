import React from 'react'
import { View, Text } from 'react-native'

const Profile = () => (
  <View style={{ flex: 1 }}>
    <View style={{ flex: 1 }} />
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <View>
        <Text>
          Profile
        </Text>
      </View>
    </View>
    <View style={{ flex: 1 }} />
  </View>
)

export default Profile
