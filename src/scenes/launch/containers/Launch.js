import React from 'react'
import { View, Text } from 'react-native'

const Launch = () => (
  <View style={{ position: 'absolute', top: 100, left: 40 }}>
    <Text>
      Launch
    </Text>
  </View>
)

export default Launch
