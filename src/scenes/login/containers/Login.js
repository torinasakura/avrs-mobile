import { connect } from 'react-redux'
import { change, login } from '../actions'
import Login from '../components/Login'

export default connect(
  state => ({
    email: state.login.email,
    password: state.login.password,
    errors: state.login.errors,
  }),
  dispatch => ({
    onChangeEmail: value => dispatch(change('email', value)),
    onChangePassword: value => dispatch(change('password', value)),
    onLogin: () => dispatch(login()),
  })
)(Login)
