import { Actions } from 'react-native-router-flux'
import * as actions from '../constants'
import { save } from '../../../actions/user'

export function change(field, value) {
  return {
    type: actions.change,
    field,
    value,
  }
}

export function login() {
  return async (dispatch, getState, { post }) => {
    const { email, password } = getState().login

    const { result, response } = await post('users/auth', { json: { email, password } })

    if (response.ok) {
      dispatch(save(result))
      Actions.main()
    } else if (response.status === 422) {
      dispatch({
        type: actions.setErrors,
        errors: result,
      })
    }
  }
}
