import React from 'react'
import { View, TextInput } from 'react-native'
import Button from 'react-native-button'

const inputStyle = {
  height: 40,
  borderColor: 'gray',
  borderWidth: 1,
  marginTop: 25,
  marginRight: 30,
  marginLeft: 30,
  borderRadius: 4,
  paddingRight: 10,
  paddingLeft: 10,
}

const Login = ({ email, password, onChangeEmail, onChangePassword, onLogin }) => (
  <View style={{ flex: 1 }}>
    <View style={{ flex: 1 }} />
    <View style={{ flex: 1 }}>
      <View>
        <TextInput
          value={email}
          style={inputStyle}
          placeholder='Логин'
          onChangeText={onChangeEmail}
        />
      </View>
      <View>
        <TextInput
          secureTextEntry
          value={password}
          style={inputStyle}
          placeholder='Пароль'
          onChangeText={onChangePassword}
        />
      </View>
      <View>
        <Button
          style={{ marginTop: 20 }}
          onPress={onLogin}
        >
          Login
        </Button>
      </View>
    </View>
    <View style={{ flex: 1 }} />
  </View>
)

export default Login
