// import * as actions from '../constants'
import { addListener } from '../../../constants/communicator'

export function listenStat() {
  return {
    type: addListener,
    event: 'stat',
  }
}
