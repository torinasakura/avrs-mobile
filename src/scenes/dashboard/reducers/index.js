import { combineReducers } from 'redux'
import stat from './stat'

export default combineReducers({
  stat,
})
