import { createReducer } from '../../../utils'
import * as communicatorActions from '../../../constants/communicator'

const initialState = {
}

export default createReducer(initialState, {
  [communicatorActions.event]: (state, { event, data }) => {
    if (event === 'stat') {
      return data
    }

    return state
  },
})
