import React from 'react'
import { View, Text } from 'react-native'

const Dashboard = ({ m, c }) => (
  <View style={{ flex: 1 }}>
    <View style={{ flex: 1 }} />
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <View>
        <Text>
          Dashboard
        </Text>
      </View>
      <View>
        <Text style={{ fontSize: 18, marginTop: 10 }}>
          Memory {m}
        </Text>
      </View>
      <View>
        <Text style={{ fontSize: 18, marginTop: 10 }}>
          CPU {c}
        </Text>
      </View>
    </View>
    <View style={{ flex: 1 }} />
  </View>
)

export default Dashboard
