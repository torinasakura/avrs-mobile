import React, { Component } from 'react'
import { connect } from 'react-redux'
import { listenStat } from '../actions'
import Dashboard from '../components/Dashboard'

class DashboardContainer extends Component {
  componentWillMount() {
    this.props.onListenStat()
  }

  render() {
    return (
      <Dashboard {...this.props} />
    )
  }
}

export default connect(
  state => state.dashboard.stat,
  dispatch => ({
    onListenStat: () => dispatch(listenStat()),
  })
)(DashboardContainer)
