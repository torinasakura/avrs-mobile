import React from 'react'
import { Text } from 'react-native'
import { connect } from 'react-redux'
import { Router as FluxRouter, Scene } from 'react-native-router-flux'
import Launch from '../scenes/launch/containers/Launch'
import Login from '../scenes/login/containers/Login'
import Dashboard from '../scenes/dashboard/containers/Dashboard'
import Services from '../scenes/services/containers/Services'
import Statistics from '../scenes/statistics/containers/Statistics'
import Profile from '../scenes/profile/containers/Profile'

const RouterWithRedux = connect()(FluxRouter)

const TabIcon = ({ title }) => (
  <Text>{title}</Text>
)

const Router = () => (
  <RouterWithRedux hideNavBar>
    <Scene
      initial
      key='launch'
      component={Launch}
    />
    <Scene
      key='login'
      direction='vertical'
      component={Login}
    />
    <Scene key='main' tabs>
      <Scene key='dashboardTab' title='Dash' icon={TabIcon} initial>
        <Scene key='dashboard' component={Dashboard} />
      </Scene>
      <Scene key='servicesTab' title='Serv' icon={TabIcon}>
        <Scene key='services' component={Services} />
      </Scene>
      <Scene key='statisticsTab' title='Stat' icon={TabIcon}>
        <Scene key='statistics' component={Statistics} />
      </Scene>
      <Scene key='profileTab' title='Prof' icon={TabIcon}>
        <Scene key='profile' component={Profile} />
      </Scene>
    </Scene>
  </RouterWithRedux>
)

export default Router
