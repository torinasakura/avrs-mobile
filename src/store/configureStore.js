import { createStore, compose, applyMiddleware } from 'redux'
import rootReducer from '../reducers'
import { communicator, api } from './middleware'

const enhancer = compose(
  applyMiddleware(api, communicator),
)

export default function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, enhancer)

  return store
}
