import { connect, addListener, removeListener, event } from '../../constants/communicator'

window.navigator.userAgent = 'ReactNative'

let socket = null
const listeners = {}

export default function communicator({ dispatch }) {
  return next => action => {
    if (action.type.includes('@@avrs-mobile/communicator')) {
      if (action.type === connect) {
        const sio = require('socket.io-client/socket.io') // eslint-disable-line global-require

        socket = sio('http://communicator.stage.aversis.net', {
          query: `token=${action.token}&type=client`,
          jsonp: false,
        })

        socket.on('connect', () => {
          // handle connect
        })

        socket.on('disconect', () => {
          // handle disconect
        })

        socket.on('error', () => {
          // handle error
        })
      }

      if (action.type === addListener) {
        listeners[action.event] = data => {
          dispatch({
            type: event,
            event: action.event,
            data,
          })
        }

        socket.on(action.event, listeners[action.event])
      }

      if (action.type === removeListener) {
        socket.off(action.event, listeners[action.event])
      }
    }

    return next(action)
  }
}
