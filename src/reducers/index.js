import { combineReducers } from 'redux'
import routes from './routes'
import user from './user'
import login from '../scenes/login/reducers'
import dashboard from '../scenes/dashboard/reducers'

export default combineReducers({
  routes,
  login,
  user,
  dashboard,
})
