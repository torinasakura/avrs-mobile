import { createReducer } from '../utils'
import * as actions from '../constants/user'

const initialState = {
  token: undefined,
}

export default createReducer(initialState, {
  [actions.load]: (state, { user }) => ({ ...state, ...user }),
  [actions.loadToken]: (state, { token }) => ({ ...state, token }),
})
