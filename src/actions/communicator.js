import * as actions from '../constants/communicator'

export function connect(token) {
  return {
    type: actions.connect,
    token,
  }
}
