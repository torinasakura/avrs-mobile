import { AsyncStorage } from 'react-native'
import { Actions } from 'react-native-router-flux'
import * as actions from '../constants/user'
import { connect } from './communicator'

window.navigator.userAgent = 'ReactNative'

export function load() {
  return async (dispatch) => {
    const token = await AsyncStorage.getItem('token')

    if (!token) {
      setTimeout(() => {
        Actions.login()
      }, 400)
    } else {
      dispatch({
        type: actions.loadToken,
        token,
      })

      dispatch(connect(token))

      setTimeout(() => {
        Actions.main()
      }, 400)
    }
  }
}

export function save(user) {
  return async () => {
    await AsyncStorage.setItem('token', user.token)
  }
}
