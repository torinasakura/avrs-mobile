import React from 'react'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import Router from './containers/Router'
import { load } from './actions/user'

const store = configureStore()

store.dispatch(load())

const App = () => (
  <Provider store={store}>
    <Router />
  </Provider>
)

export default App
