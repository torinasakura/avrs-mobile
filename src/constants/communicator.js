export const connect = '@@avrs-mobile/communicator/CONNECT'
export const addListener = '@@avrs-mobile/communicator/ADD_LISTENER'
export const removeListener = '@@avrs-mobile/communicator/REMOVE_LISTENER'
export const event = '@@avrs-mobile/communicator/EVENT'
